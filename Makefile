OBJ_DIR=.
SRC_DIR=.
INC_DIR=.
LIBS_DIR=

INCLUDES=
LIBS=

CC=gcc
LD=gcc
CFLAGS=-c -O3 -g 


_DEPS=addresstranslation.h
DEPS=$(patsubst %,$(INC_DIR)/%,$(_DEPS))

_OBJ=addresstranslation.o TestAddressTranslation.o
OBJ=$(patsubst %,$(OBJ_DIR)/%,$(_OBJ))



$(OBJ_DIR)/%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -o $@ $< $(INCLUDES)
	

TestAddressTranslation: $(OBJ)
	$(CC) -o $@ $^ $(LIBS)
	
.PHONY: clean

clean:
	rm -f $(OBJ_DIR)/*.o *~ core $(SRC_DIR)/*~ TestAddressTranslation 