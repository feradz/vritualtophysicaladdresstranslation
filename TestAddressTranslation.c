#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include "addresstranslation.h"

int main(int argc, char* argv[])
{
   int *a1, *a2, *b1, *b2 = NULL;
   int N = 8192;
   uintptr_t ap1, ap2, bp1, bp2 = 0;

   printf("Test virtual to physical address translation.\n");

   a1 = (int*)calloc(sizeof(int), N);
   if (!a1)
   {
      printf("Error: cannot allocate memory for a\n");
      return 1;
   }

   b1 = mmap(0, sizeof(int)*N, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
   memset(b1, 0, sizeof(int)*N);
   //b1 = (int*)malloc(sizeof(int) * N);
   if (!b1)
   {
      printf("Error: cannot allocate memory for b\n");
      return 1;
   }

   ap1 = virtual_to_physical_address((uintptr_t)a1);
   bp1 = virtual_to_physical_address((uintptr_t)b1);

   printf("a1_virt= %p a1_phys= %" PRIxPTR "\n", a1, ap1);
   printf("b1_virt= %p b1_phys= %" PRIxPTR "\n", b1, bp1);

   a2 = a1 + 2000;
   b2 = b1 + 6;

   ap2 = virtual_to_physical_address((uintptr_t)a2);
   bp2 = virtual_to_physical_address((uintptr_t)b2);

   printf("a2_virt= %p a2_phys= %" PRIxPTR "\n", a2, ap2);
   printf("b2_virt= %p b2_phys= %" PRIxPTR "\n", b2, bp2);

   printf("Done\n");
}
